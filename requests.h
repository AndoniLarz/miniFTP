/*
 * requests.h
 * Xavier Delord
 * 12.11.2002
 */

#define MAXPATH 256

/* Client requests */
#define REQUEST_PUT 1
#define REQUEST_GET 2
#define REQUEST_DEL 3
#define REQUEST_DIR 4

/* Server answers */
#define ANSWER_OK      0
#define ANSWER_UNKNOWN 1        /* unknown request */
#define ANSWER_ERROR   2        /* error while processing request */

struct request {
    /* L'entier kind doit être REQUEST_PUT, REQUEST_GET, REQUEST_DEL ou
    REQUEST_DIR.*/
    int  kind;

    /*La chaîne path contient le nom du fichier à écrire (PUT), à lire (GET), à
    détruire (DEL) ou à lister (DIR).*/
    char path[MAXPATH];

    /* Lors d'un PUT, l'entier nbbytes contient la taille du
    fichier.*/
    int  nbbytes;               /* for PUT only */
};

struct answer {
    // accusé de réception. Si ack > 0 -> ack = ANSWER_OK
    int  ack;

    // nbbytes contient la taille du fichier que le serveur va envoyer.
    int  nbbytes;               /* for GET only */

    //numero d'erreur
    int  errnum;                /* significant if ack == ERROR and != 0 */
};

/* Port number deduced from UID allowing many users on a sigle host */
#define PORT (IPPORT_USERRESERVED + getuid())
