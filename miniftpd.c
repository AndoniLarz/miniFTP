//THIS IS THE SERVER

/*
 * miniftpd.c
 * Xavier Delord
 * 12.11.2002
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/wait.h>

#include "requests.h"
#include "common.h"


/* Mettre 1 avec un debugger. */
#if 0
#define fork() (0)
#endif



void traitementSigChild(int sig)
{

    printf("Signal SIGCHILD reçu ! \n");

    int status;
    wait(&status);

    /* réactivation du traitement */
    signal(SIGCHLD, traitementSigChild);
    printf("Traitement réactivé \n");
}


/* get_callername(fd) returns the hostname (or its ip address) on the other side
 * of the  connected socket fd. The name is contained in a static area.
 * (note: this is rather tricky)
 * This is used only to print nice messages about what's going on.
 */
char *get_callername (int fd)
{
    struct sockaddr_in sins;
    int len;
    struct hostent *sp;

    len = sizeof (sins);
    if (getpeername (fd, (struct sockaddr *)&sins, &len) == -1) {
        perror ("getpeername");
        return "Noname";
    }
    sp = gethostbyaddr ((char *)&(sins.sin_addr.s_addr), 4, AF_INET);
    if (sp == NULL) {
        char *rep;
        rep = inet_ntoa (sins.sin_addr);
        fprintf (stderr, "can't reverse address for %s\n", rep);
        return rep;
    }
    return sp->h_name;
} /* get_callername */


/*
 * Reception d'un fichier (requete PUT).
 * 'clientfd' est un socket connecte a un client.
 * 'filename' est le nom du fichier a creer.
 * 'length' est le nombre d'octets attendus.
 */
void put_file (int clientfd, char *filename, int length)
{

    struct answer reponseServeur;

    printf("Nom du fichier a ecrire : %s \n", filename);

    int fichierAEcrire = open(filename, O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);

    if (fichierAEcrire < 0) {
        reponseServeur.errnum = ANSWER_ERROR;
        reponseServeur.errnum = errno;
        reponseServeur.nbbytes = 0;

        perror("Erreur, descripteur de fichier < 0");
    }
    else {
        reponseServeur.ack = ANSWER_OK ;
        reponseServeur.errnum = 0;
        reponseServeur.nbbytes = 0;
    }

    if(write(clientfd, &reponseServeur, sizeof(reponseServeur)) != -1) {
        if(reponseServeur.ack == ANSWER_OK){
            printf("On a renvoyé la réponse serveur, on est prêt à recevoir les données à écrire dans le fichier \n");

            if((copy_n_bytes(clientfd, fichierAEcrire, length)) != 0) {
                perror2("Erreur lors de l'écriture des données dans le fichier",filename);
                exit(errno);
            }

            if(close(fichierAEcrire) == -1){
                perror2("Erreur lors de la fermeture du fichier", filename);
                exit(errno);
            }
        }
    }
    else {
        perror("Erreur lors de l'envoi de la réponse serveur");
        exit(errno);
    }
} /* put_file */


/*
 * Envoi d'un fichier (requete GET).
 * 'clientfd' est un socket connecte a un client.
 * 'filename' est le nom du fichier a envoyer.
 */
void get_file (int clientfd, char *filename)
{
    struct answer reponseServeur;
    struct stat statutFichier;

    if (stat(filename, &statutFichier) == -1) {
        reponseServeur.errnum = errno;
        reponseServeur.ack=ANSWER_ERROR;
        reponseServeur.nbbytes=0;

        perror2("Erreur lors du stat", filename);
    }
    else {
        reponseServeur.ack = ANSWER_OK ;
        reponseServeur.errnum = 0;
        reponseServeur.nbbytes = (int) statutFichier.st_size;
    }

    if(write(clientfd, &reponseServeur, sizeof(reponseServeur)) != -1) {
        if(reponseServeur.ack==ANSWER_OK){
            printf("Bonne reponse serv \n");
            if(S_ISREG(statutFichier.st_mode)) {
                int fichierAOuvrir = open(filename, O_RDONLY);

                if(fichierAOuvrir < 0) {
                    perror("Erreur, descripteur de fichier < 0");
                    exit(errno);
                }

                if((copy_n_bytes(fichierAOuvrir, clientfd, statutFichier.st_size)) != 0) {
                    perror("Erreur lors de l'envoi des données au client");
                    exit(errno);
                }

                if(close(fichierAOuvrir) == -1) {
                    perror2("Erreur à la fermeture", filename);
                    exit(errno);
                }
            }
            else {
                perror2("Pas un fichier basique", filename);
                exit(errno);
            }
        }
    }
    else {
        perror("Erreur lors de l'envoi de la réponse serveur");
        exit(errno);
    }
} /* get_file */


/*
 * Destruction d'un fichier (requete DEL).
 * 'clientfd' est un socket connecte a un client.
 * 'filename' est le nom du fichier a detruire.
 */
void del_file (int clientfd, char *filename)
{
    struct answer reponseServeur;
    struct stat statutFichier;

    reponseServeur.nbbytes = 0 ;

    if (stat(filename, &statutFichier) == -1) {
        reponseServeur.ack = ANSWER_ERROR ;
        reponseServeur.errnum = errno;
        perror2("Erreur stat", filename);
    }

    else if(S_ISDIR(statutFichier.st_mode) || !(S_ISREG(statutFichier.st_mode))) {
        reponseServeur.ack = ANSWER_ERROR ;
        reponseServeur.errnum = errno;
        perror2("Pas un fichier", filename);
    }
    else {
        reponseServeur.ack = ANSWER_OK ;
        reponseServeur.errnum = 0;
    }

    if(write(clientfd, &reponseServeur, sizeof(reponseServeur)) != -1) {
        if(reponseServeur.ack == ANSWER_OK) {
            if(unlink(filename) != 0) {
                perror2("Erreur avec unlink", filename);
            }
        }
    }
    else {
        perror("Erreur lors de l'envoi de la réponse serveur");
        exit(errno);
    }
} /* del_file */

/*
 * ls sur un fichier/repertoire (requete DIR).
 * 'clientfd' est un socket connecte a un client.
 * 'pathname' est le nom du fichier ou repertoire a lister.
 */
void dir_file (int clientfd, char *pathname)
{

    struct answer reponseServeur;
    struct stat statutDossier;

    reponseServeur.nbbytes = 0 ;

    if (stat(pathname, &statutDossier) == -1) {
        reponseServeur.ack = ANSWER_ERROR ;
        reponseServeur.errnum = errno;
        perror2("Erreur stat", pathname);
    }

    else if(!(S_ISDIR(statutDossier.st_mode))) {
        reponseServeur.ack = ANSWER_ERROR ;
        reponseServeur.errnum = errno;
        perror2("Pas un dossier", pathname);
    }
    else {
        reponseServeur.ack = ANSWER_OK ;
        reponseServeur.errnum = 0;
    }

    if(write(clientfd, &reponseServeur, sizeof(reponseServeur)) != -1) {

        // On prépare les arguments pour le exec
        char* arguments[] = {"ls", "-l", pathname, NULL};

        // On redirige depuis la sortie standard sur clientfd
        if(dup2(clientfd, STDOUT_FILENO) != -1) {
            // On redirige depuis la sortie d'erreur sur clientfd
            if(dup2(clientfd, STDERR_FILENO) != -1) {
                if(execv("/bin/ls", arguments) == -1) {
                    perror("Erreur lors du execv de ls -l");
                    exit(errno);
                }
            }
            else {
                perror("Erreur lors de la redirection de la sortie d'erreur sur clientfd");
                exit(errno);
            }
        }
        else {
            perror("Erreur lors de la redirection de la sortie standard sur clientfd");
            exit(errno);
        }
    }
    else {
        perror("Erreur lors de l'envoi de la réponse serveur");
        exit(errno);
    }
} /* dir_file */



/*
 * Lit une requete sur le descripteur 'f', et appelle la procedure
 * correspondante pour gerer cette requete.
 */
void handle_request (int f)
{
    struct request r;

    printf ("Process %d, handling connection from %s with fd %d \n",
            getpid(), get_callername (f), f);

    read(f, (void *)&r, sizeof(r));

    switch(r.kind) {
        case REQUEST_GET :
            printf("Le client veut get \n");
            printf("Le fichier voulu est : %s \n", r.path);
            get_file(f, r.path);
            break;

        case REQUEST_PUT :
            printf("Le client veut putxe \n");
            put_file(f, r.path, r.nbbytes);
            break ;

        case REQUEST_DIR :
            printf("Le client veut dir \n");
            dir_file(f, r.path);
            break ;

        case REQUEST_DEL :
            printf("Le client veut del \n");
            del_file(f, r.path);
            break ;

        default :
            printf("Le client a envoyé une requete inconnue");
            struct answer reponseServeur;
            reponseServeur.ack = ANSWER_UNKNOWN;
            reponseServeur.errnum = 0;
            reponseServeur.nbbytes = 0;
            write(f,&reponseServeur, sizeof(reponseServeur));
            break;
    }
}


int main ()
{
    struct sockaddr_in soc_in;
    int val;
    int ss;                     /* socket d'ecoute */
    int tailleSocin = sizeof(soc_in);
    pid_t pid;

    /* creation d'un socket 'ss' : famille IP et type TCP */

    ss = socket(AF_INET, SOCK_STREAM, 0);

    /* Force la reutilisation de l'adresse si non allouee */
    val = 1;
    if (setsockopt(ss,SOL_SOCKET,SO_REUSEADDR,(char*)&val,sizeof(val)) == -1) {
        perror ("setsockopt");
        exit (1);
    }

    /*
     * Nomme localement le socket :
     * socket inet, port local PORT, adresse IP locale quelconque
     */

    memset(&soc_in, '0', sizeof(soc_in));

    soc_in.sin_family = AF_INET ;
    soc_in.sin_addr.s_addr = htonl(INADDR_ANY);
    soc_in.sin_port = htons(PORT);


    /* Prepare le socket a la reception de connexions */
    bind(ss, (struct sockaddr*)&soc_in, sizeof(soc_in));
    listen(ss, 150);
    printf("On écoute \n");

    signal(SIGCHLD, traitementSigChild);

    while (1) {

        val = accept(ss, (struct sockaddr*)&soc_in, &tailleSocin);

        pid = fork();

        if(pid == 0) {
            handle_request(val);
            close(val);
            exit(EXIT_SUCCESS);
        }
        close(val);
    } /* while (1) */
    /* NOTREACHED */
    return 0;
}
