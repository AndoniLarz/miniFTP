//THIS IS THE CLIENT

/*
 * miniftp.c
 * Xavier Delord
 * 12.11.2002
 */

/*
 * miniftp hostname get distfile  localfile
 * miniftp hostname put localfile distfile
 * miniftp hostname del file
 * miniftp hostname dir file
 */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <sys/wait.h>

#include "common.h"
#include "requests.h"

/*
 * check_answer ne rend la main que si  a->ack est ANSWER_OK,
 * sinon le processus se termine avec un message d'erreur.
 */
void check_answer (struct answer *a)
{
    switch (a->ack) {
      case ANSWER_OK:
        break;
      case ANSWER_UNKNOWN:
        fprintf (stderr, "?? unknown request ?\n");
        exit (1);
      case ANSWER_ERROR:
        if (a->errnum != 0) {
            errno = a->errnum;
            perror ("Server rejection");
        } else {
            fprintf (stderr, "Server rejection\n");
        }
        exit (1);
      default:
        fprintf (stderr, "Bad answer from server %d\n", a->ack);
        exit (1);
    }
}

/*
 * Obtention d'un fichier distant.
 * 'serverfd' est un socket connecte a la machine 'servername'.
 * 'distname' est le nom du fichier a demander, 'localname' est le nom du
 * fichier resultat.
 */
void get_file (int serverfd, char *servername, char *distname, char *localname)
{

    int descriptorLocal;
    //définition de la requête
    struct request maRequete;
    struct answer maReponse;

    // création ou écrasement du fichier local
    descriptorLocal = open (localname, O_WRONLY |  O_CREAT | O_TRUNC, 0644);
    if(descriptorLocal == -1){ //vérification du bon fonctionnement
        printf("Erreur lors de l'ouverture du fichier local \n");
        exit(errno);
    }

    // initialisation de la requête
    maRequete.kind = REQUEST_GET; //Type de la requête
    strcpy(maRequete.path, distname); //copie du nom du fichier dans le path de la requête

    //on demande au serveur le fichier voulu (ecriture dans la socket)
    if(write(serverfd, &maRequete, sizeof(maRequete)) == -1){
        printf("Erreur lors de l'envoi de la requête \n");
        exit(errno); //on sort du programme si erreur lors de l'écriture
    }

    printf("Requête GET envoyée pour le fichier : %s \n", maRequete.path);

    // on lit la socket et la structure maReponse
    if(read(serverfd, &maReponse, sizeof(maReponse)) == -1){
        printf("Erreur lors de la récupération de la réponse serveur \n");
        exit(errno); //on sort du programme si erreur lors de l'écriture
    }

    check_answer (&maReponse); //appel de la fonction check_answer qui termine le process si réponse != OK

    printf("Réponse vérifiée, démarrage de l'échange de données\n");

    //copie du contenu de la socket vers fichier local
    copy_n_bytes (serverfd, descriptorLocal, maReponse.nbbytes);

    printf("Fichier bien reçu depuis le serveur et copié sur le fichier suivant : %s \n", localname);

}


/*
 * Envoi d'un fichier a distance.
 * 'serverfd' est un socket connecte a la machine 'servername'.
 * 'localname' est le nom du fichier a envoyer, 'distname' est le nom du
 * fichier resultat (sur la machine distante).
 */
void put_file (int serverfd, char *servername, char *localname, char *distname)
{

    int descriptorLocal;
    //définition de la requête
    struct request maRequete;
    struct answer maReponse;
    struct stat statutFichier;

    if (stat(localname, &statutFichier) == -1) { //récupération des infos du fichier (taille, nom, propriétaire, ...)
        perror("Erreur lors de la récupération du fichier local.");
        exit(errno);
    }

    // initialisation de la requête
    maRequete.kind = REQUEST_PUT; //Type de la requête
    strcpy(maRequete.path, distname); //copie du nom du fichier dans le path de la requête
    maRequete.nbbytes = (int) statutFichier.st_size; //calcul de la taille du fichier local

    //on donne le nom du fichier où écrire sur le serveur (ecriture dans la socket)
    if(write(serverfd, &maRequete, sizeof(maRequete)) == -1){
        perror("Erreur lors de l'envoi de la requête.");
        exit(errno); //on sort du programme si erreur lors de l'écriture
    }

    printf("Requête PUT envoyée pour le fichier : %s \n", maRequete.path);

    // on lit la socket et la structure maReponse
    if(read(serverfd, &maReponse, sizeof(maReponse)) == -1){
        perror("Erreur lors de la récupération de la réponse serveur.");
        exit(errno); //on sort du programme si erreur lors de l'écriture
    }

    //appel de la fonction check_answer qui termine le process si réponse != OK
    check_answer (&maReponse);

    printf("Réponse vérifiée, démarrage de l'échange de données\n");

    if(S_ISREG(statutFichier.st_mode)) {
        // Ouverture du fichier local
        descriptorLocal = open (localname, O_RDONLY);
        if(descriptorLocal == -1){ //vérification du bon fonctionnement
            perror("Erreur lors de l'ouverture du fichier local.");
            exit(errno);
        }
        printf("Fichier %s ouvert \n", localname);

        // On envoie le contenu du fichier local
        if(copy_n_bytes(descriptorLocal, serverfd, maRequete.nbbytes) != 0) {
            perror("Erreur lors de l'envoi des données.");
            exit(errno); //on sort du programme si erreur lors de l'écriture
        }
        printf("On a envoyé %d octets au serveur sur le descripteur %d à partir du descripteur %d \n", maRequete.nbbytes, serverfd, descriptorLocal);

        if(close(descriptorLocal) == -1) {
            perror2("stat", localname);
            exit(errno);
        }

        printf("Données du fichier local ' %s ' copiées sur le serveur dans le fichier : %s \n", localname, distname);
    }
    else {
        printf("Erreur, %s n'est pas un fichier", localname);
    }
}


/*
 * Destruction d'un fichier distant.
 * 'serverfd' est un socket connecte a la machine 'servername'.
 * 'distname' est le nom du fichier a detruire.
 */
void del_file (int serverfd, char *servername, char *distname)
{
    struct request maRequete;
    struct answer maReponse;

    maRequete.kind = REQUEST_DEL; //Type de la requête
    strcpy(maRequete.path, distname); //copie du nom du fichier dans le path de la requête

    printf("fichier à supprimer : %s \n", maRequete.path);

    //on envoie au serveur le fichier à supprimer (ecriture dans la socket)
    if(write(serverfd, &maRequete, sizeof(maRequete)) == -1){
        perror("Erreur lors de l'envoi de la requête.");
        exit(errno); //on sort du programme si erreur lors de l'écriture
    }

    printf("Requête DEL envoyée pour le fichier : %s \n", maRequete.path);

    // on lit la socket et la structure maReponse
    if(read(serverfd, &maReponse, sizeof(maReponse)) == -1){
        perror("Erreur lors de la récupération de la réponse serveur.");
        exit(errno); //on sort du programme si erreur lors de l'écriture
    }

    check_answer (&maReponse); //appel de la fonction check_answer qui termine le process si réponse != OK

    printf("Le fichier ' %s ' a bien été supprimé sur le serveur\n", maRequete.path);

}

/*
 * Obtention d'un dir distant.
 * 'serverfd' est un socket connecte a la machine 'servername'.
 * 'distname' est le nom du repertoire distant a lister.
 */
void dir_file (int serverfd, char *servername, char *distname)
{

    //définition de la requête
    struct request maRequete;
    struct answer maReponse;

    maRequete.kind = REQUEST_DIR;
    strcpy(maRequete.path, distname); //copie du nom du dossier dans le path de la requête

    //on envoie au serveur le dossier à lister (ecriture dans la socket)
    if(write(serverfd, &maRequete, sizeof(maRequete)) == -1){
        perror("Erreur lors de l'envoi de la requête.");
        exit(errno); //on sort du programme si erreur lors de l'écriture
    }

    printf("Requête DIR envoyée pour le fichier : %s \n", maRequete.path);

    // on lit la socket et la structure maReponse
    if(read(serverfd, &maReponse, sizeof(maReponse)) == -1){
        perror("Erreur lors de la récupération de la réponse serveur.");
        exit(errno); //on sort du programme si erreur lors de l'écriture
    }

    check_answer (&maReponse); //appel de la fonction check_answer qui termine le process si réponse != OK

    printf("Réponse vérifiée, démarrage de l'échange de données\n");
    printf("Liste associée à : %s \n", maRequete.path);

    copy_all_bytes(serverfd, STDOUT_FILENO); // on affiche la liste associée au répertoire distname

}

/*
 * Retourne un socket connecte la machine 'serverhost' sur le port 'port',
 * ou termine le processus en cas d'echec.
 */
int connection (char *serverhost, unsigned int port) {

    int socketToSend = 0;
    struct sockaddr_in serv_addr;

    // création de la socket
    if ((socketToSend = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        exit(-1);

    // raz de la structure serv_addr
    memset(&serv_addr, '0', sizeof(serv_addr));
    // famille d'adresses IPv4
    serv_addr.sin_family = AF_INET;
    // port associé
    serv_addr.sin_port = htons(port);
    // test de la validité de l'adresse du serveur
    if (inet_pton(AF_INET, serverhost, &serv_addr.sin_addr) == -1)
        exit(-1);
    else if (inet_pton(AF_INET, serverhost, &serv_addr.sin_addr) == 0)
    {
        perror("Format de l'adresse incorrect.");
        exit(-1);
    }

    // connexion à la socket
    if(connect(socketToSend, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) == -1)
    {
        perror("Connexion non réussie.");
        exit(-1);
    }

    //return le_socket_connecte_au_serveur;
    return socketToSend;

}


void usage (void)
{
    fprintf (stderr, "miniftp: hostname get distfilename localfilename\n");
    fprintf (stderr, "miniftp: hostname put localfilename distfilename\n");
    fprintf (stderr, "miniftp: hostname del distfilename\n");
    fprintf (stderr, "miniftp: hostname dir pathname\n");
    exit (2);
}

int main (int argc, char **argv)
{
    char *serverhost;
    int cmde;
    int serverfd;

    if (argc < 3)
      usage ();

    serverhost = argv[1];

    if ((strcmp (argv[2], "put") == 0) && (argc == 5))
      cmde = REQUEST_PUT;
    else if ((strcmp (argv[2], "get") == 0) && (argc == 5))
      cmde = REQUEST_GET;
    else if ((strcmp (argv[2], "del") == 0) && (argc == 4))
      cmde = REQUEST_DEL;
    else if ((strcmp (argv[2], "dir") == 0) && (argc == 4))
      cmde = REQUEST_DIR;
    else
      usage();

    serverfd = connection (serverhost, PORT);
    printf("Connexion réussie sur la socket %d \n", serverfd);

    switch (cmde) {
      case REQUEST_GET:
        get_file (serverfd, serverhost, argv[3], argv[4]);
        break;
      case REQUEST_PUT:
        put_file (serverfd, serverhost, argv[3], argv[4]);
        break;
      case REQUEST_DEL:
        del_file (serverfd, serverhost, argv[3]);
        break;
      case REQUEST_DIR:
        dir_file (serverfd, serverhost, argv[3]);
        break;
    }

    close (serverfd);
    return 0;
}

